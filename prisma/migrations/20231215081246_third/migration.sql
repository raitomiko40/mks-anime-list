/*
  Warnings:

  - You are about to drop the column `anime_imaga` on the `Collection` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Collection" DROP COLUMN "anime_imaga",
ADD COLUMN     "anime_image" TEXT;
