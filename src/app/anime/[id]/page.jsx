import { getAnimeResponse } from '@/libs/api-libs'
import VideoPlayer from '@/components/Utilities/VideoPlayer'
import Image from 'next/image'
import React from 'react'
import CollectionButton from '@/components/AnimeList/CollectionButton'
import { authUserSession } from '@/libs/auth-libs'
import prisma from '@/libs/prisma'

const page = async ({params : {id}}) => {

    const anime = await getAnimeResponse(`anime/${id}`)
    const user = await authUserSession()
    const collection = await prisma.collection.findFirst({
        where : { 
            user_email: user?.email,
            anime_mal_id: id
        }
    })

  return (
    <>
    <div className='py-4 px-4'>
        <h3 className='text-2xl text-color-primary'>{anime.data.title} - {anime.data.year}</h3>
        {!collection && user && 
        <CollectionButton 
            anime_mal_id={id} 
            user_email={user?.email} 
            anime_title={anime.data.title}
            anime_image={anime.data.images.webp.image_url}
            />
        }
    </div>
    <div className='p-4 flex gap-4 text-color-primary overflow-x-auto'>
        <div className='w-36 flex flex-col justify-center items-center rounded border border-color-primary'>
            <h3>Peringkat</h3>
            <p>{anime.data.rank}</p>
        </div>
        <div className='w-36 flex flex-col justify-center items-center rounded border border-color-primary'>
            <h3>score</h3>
            <p>{anime.data.score}</p>
        </div>
        <div className='w-36 flex flex-col justify-center items-center rounded border border-color-primary'>
            <h3>Anggota</h3>
            <p>{anime.data.members}</p>
        </div>
        <div className='w-36 flex flex-col justify-center items-center rounded border border-color-primary'>
            <h3>Episode</h3>
            <p>{anime.data.episodes}</p>
        </div>
    </div>
    <div className='flex px-4 sm:flex-nowrap flex-wrap gap-4 text-color-primary'>
        <Image
        src={anime.data.images.webp.image_url}
        alt={anime.data.images.jpg.image_url}
        width={250}
        height={250}
        className='w-full rounded object-cover'
        />
        <p className='text-justify md:text-xl text-md'>{anime.data.synopsis}</p>
    </div>
    <div>
        <VideoPlayer youtubeId={anime.data.trailer.youtube_id} />
    </div>
    </>
  )
}

export default page