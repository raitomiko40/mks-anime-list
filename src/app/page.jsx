import AnimeList from "@/components/AnimeList"
import Header from "@/components/AnimeList/Header"
import { getAnimeResponse, getNestedAnimeResponse, reProduce } from "../libs/api-libs"

const Home = async () => {
  
  const topAnime = await getAnimeResponse("top/anime", "limit=8")
  let recommendedAnime = await getNestedAnimeResponse("recommendations/anime", "entry")
  recommendedAnime = reProduce(recommendedAnime, 4)
  
  return (
    <>
    {/* anime terpopuler */}
      <section>
        <Header  title="Paling Populer" linkTitle="Lihat Semua" linkHref="/populer"/>
        <AnimeList api={topAnime}/>
      </section>
      {/* rekomendasi anime */}
      <section>
        <Header  title="Rekomendasi" />
        <AnimeList api={recommendedAnime}/>
      </section>
    </>
  )
}

export default Home
