import { authUserSession } from "@/libs/auth-libs"
import Image from "next/image"
import Link from "next/link"

const Dashboard = async () => {
  const user = await authUserSession()
  return (
    <div className="flex justify-center items-center p-4 text-color-primary flex-col mt-8">
      <h5 className="text-2xl font-bold p-4" >Welcome, {user.name}</h5>
      <Image
          className="rounded " 
          src={user.image} 
          width={250}
          height={250}
          alt="..." />
      <div className="p-4 flex gap-4">
          <Link 
              className="bg-color-accent text-color-dark font-bold px-4 py-3 rounded text-xl  "
              href="/users/dashboard/collection">
              My Collection
          </Link>
          <Link 
              className="bg-color-accent text-color-dark font-bold px-4 py-3 rounded text-xl  "
              href="/users/dashboard/comment">
              My Comment
          </Link>
      </div>
    </div>
  )
}

export default Dashboard