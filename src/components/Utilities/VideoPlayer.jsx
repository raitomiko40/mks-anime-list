'use client'

import { XCircle } from "@phosphor-icons/react"
import { useState } from "react"
import YouTube from "react-youtube"

const VideoPlayer = ({youtubeId}) => {
    const [isOpen, setIsOpen] = useState(true)
    const options = {
        width: "300",
        height: "250"
    }

const handleTrailerButton = () => {
    setIsOpen((prevState) => !prevState)
}

  return (
    <>
        { isOpen ? (
            <div className="fixed bottom-4 right-4">
            <button 
                onClick={handleTrailerButton}
                className="text-color-primary float-right bg-color-secondary mb-1">
                <XCircle size={32} />
            </button>
            <YouTube 
                videoId={youtubeId}
                onReady={(event) => event.target.pauseVideo()}
                opts={options}
                onError={() => alert("Video is broken, please try another")}
            />
        </div>
        ) : (

        <button 
            className="fixed bottom-5 right-5 w-32 bg-color-primary text-color-dark rounded hover:text-color-accent hover:bg-color-secondary transition-all"
            onClick={handleTrailerButton}>
            Trailer
        </button>
        )  }
    </>
  )
}

export default VideoPlayer