"use client"

import { useState } from "react"

const CollectionButton = ({anime_mal_id, user_email, anime_image, anime_title}) => {
    const [isCreated, setIsCreated] = useState(false)

    const handlerCollection = async(event) => {
        event.preventDefault()

        const data = { anime_mal_id, user_email, anime_image, anime_title }

        const response = await fetch("/api/v1/collection", {
          method: "POST",
          body: JSON.stringify(data)
        })
        const collection = await response.json()
        
        if(collection.isCreated) {
          setIsCreated(true)
        }
        return
    }

  return (
    <div>
        {isCreated ?
         <p className="text-color-primary"> Berhasil Di Tambahkan Ke Koleksi</p>
        :
        <button 
            onClick={handlerCollection}
            className='px-2 py-1 bg-color-accent'>
                Add To Collection
        </button>
        }
    </div>
  )
}

export default CollectionButton